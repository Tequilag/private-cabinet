//
//  EditViewController.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "EditViewController.h"
#import "EditView.h"
#import "ImageViewController.h"
#import "CountryViewController.h"
@interface EditViewController () <UITextFieldDelegate, UITextViewDelegate, LoadImageDelegate>

@property (nonatomic, strong) EditView *editView;
@property (nonatomic,assign) BOOL validating;

@end

@implementation EditViewController

- (void)loadView {
    self.editView = [[EditView alloc] init];
    self.view = self.editView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _validating=NO;
    self.editView.firstName.text=self.item.firstName;
    self.editView.firstName.delegate=self;
    self.editView.lastName.text=self.item.lastName;
    self.editView.lastName.delegate=self;
    self.editView.middleName.text=self.item.middleName;
    self.editView.middleName.delegate=self;
    self.editView.phone.text=self.item.phone;
    self.editView.phone.delegate=self;
    self.editView.email.text=self.item.email;
    self.editView.email.delegate=self;
    self.editView.country.text=self.item.country;
    self.editView.country.delegate=self;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.editView.iconButton addTarget:self action:@selector(editImage:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveItem:)];
}

- (void)editImage:(UIButton *)sender {
    ImageViewController *controller = [[ImageViewController alloc] init];
    controller.delegate=self;
    controller.PhotoFileName = self.item.imageFileName;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.editView.topOffset = self.topLayoutGuide.length;
}
-(void) textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag==6){
        CountryViewController *countryViewController =[[CountryViewController alloc] init];
        [self.navigationController pushViewController:countryViewController animated:YES ];}
}
-(BOOL) textFieldShouldEndEditing:(UITextField *)textField{
    BOOL valid=NO;
    switch (textField.tag) {
        case 1:{
            if([textField.text isEqualToString:@""])
            {
                valid=NO;//textField.backgroundColor=[UIColor redColor];
            }
            else valid=YES;
        }
            break;
        case 2:{
            if([textField.text isEqualToString:@""])
            {
                valid=NO;//textField.backgroundColor=[UIColor redColor];
            }
            else valid=YES;
        }
            break; case 3:{
                if([textField.text isEqualToString:@""])
                {
                    valid=NO;//textField.backgroundColor=[UIColor redColor];
                }
                else valid=YES;
            }
            break;
        case 4:{
                            //BOOL stricterFilter = NO;
            // NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
            //NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
            NSString *emailRegex = @"\\+{0,1}+[0-9]{1}+\\({0,1}+[0-9]{3}+\\){0,1}+[0-9]{3}+\\-{0,1}+[0-9]{2}+\\-{0,1}+[0-9]{2}$";//@"^(\\+7|8)(\\(\\d{3}\\)|\\d{3})\\d{7}$";
            NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            valid=[emailTest evaluateWithObject:textField.text];
            
            }
            break;
        case 5:{
            BOOL stricterFilter = NO;
            NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
            NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
            NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
            NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
            valid=[emailTest evaluateWithObject:textField.text];

            }
            break;
        default:
            valid=YES;
    }
    if (valid==YES)
        textField.backgroundColor=[UIColor whiteColor];
    else
        textField.backgroundColor=[UIColor redColor];
    return valid;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return text.length <= 30;
}
-(void)LoadImage:(UIImage *)image{
    self.editView.image=image;
}
- (void)saveItem:(id)sender {
    self.editView.imageFileName=@"ImageFile";
   // self.validating=[self.editView.firstName textF]
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    [UIImagePNGRepresentation(self.editView.image) writeToFile:[path stringByAppendingPathComponent:self.editView.imageFileName]
                                                    atomically:YES];
    self.item.firstName = self.editView.firstName.text;
    self.item.lastName = self.editView.lastName.text;
    self.item.middleName = self.editView.middleName.text;
    self.item.phone = self.editView.phone.text;
    self.item.email = self.editView.email.text;
    self.item.country = self.editView.country.text;
    self.item.imageFileName=self.editView.imageFileName;
    if (self.delegate) {
        [self.delegate editViewController:self didCreateItem:self.item];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
