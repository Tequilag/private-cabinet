//
//  EditView.h
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditView : UIView

@property (nonatomic, strong) UIButton *iconButton;
@property (nonatomic, strong) UITextField *firstName;
@property (nonatomic, strong) UITextField *lastName;
@property (nonatomic, strong) UITextField *middleName;
@property (nonatomic, strong) UITextField *email;
@property (nonatomic, strong) UITextField *phone;
@property (nonatomic, strong) UITextField *country;
@property (nonatomic, strong) NSString *imageFileName;
@property (nonatomic, strong) UIImage *image;

@property (nonatomic, assign) CGFloat topOffset;
@end
