//
//  EditView.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "EditView.h"

@implementation EditView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _iconButton = [[UIButton alloc] init];
        //[self.iconButton setTitle:@"Edit" forState:UIControlStateNormal];
        //[_iconButton setImage:[UIImage imageNamed:@"addIcom"] forState:UIControlStateNormal];
        [self addSubview:_iconButton];
        
        _firstName = [[UITextField alloc] init];
        _firstName.borderStyle = UITextBorderStyleRoundedRect;
        _firstName.tag=1;
        
        [self addSubview:_firstName];
        _lastName = [[UITextField alloc] init];
        _lastName.borderStyle = UITextBorderStyleRoundedRect;
        _lastName.tag=2;
        [self addSubview:_lastName];
        _middleName = [[UITextField alloc] init];
        _middleName.borderStyle = UITextBorderStyleRoundedRect;
        _middleName.tag=3;
        [self addSubview:_middleName];
        _phone = [[UITextField alloc] init];
        _phone.borderStyle = UITextBorderStyleRoundedRect;
        _phone.tag=4;
        [self addSubview:_phone];
        _email = [[UITextField alloc] init];
        _email.borderStyle = UITextBorderStyleRoundedRect;
        _email.tag=5;
        [self addSubview:_email];
        _country = [[UITextField alloc] init];
        _country.tag=6;
        _country.borderStyle = UITextBorderStyleRoundedRect;
        
        [self addSubview:_country];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    CGFloat offset = 10.0f;
    self.iconButton.frame = CGRectMake(offset,offset+ self.topOffset, 150.0f, 150.0f);
    self.lastName.frame = CGRectMake(self.iconButton.frame.origin.x + self.iconButton.bounds.size.width + offset, offset+self.topOffset, self.bounds.size.width - self.iconButton.frame.origin.x - self.iconButton.bounds.size.width - 2*offset, 40.0f);
    self.firstName.frame = CGRectMake(self.iconButton.frame.origin.x + self.iconButton.bounds.size.width + offset, offset+self.lastName.frame.origin.y +self.lastName.bounds.size.height, self.bounds.size.width - self.iconButton.frame.origin.x - self.iconButton.bounds.size.width - 2*offset, 40.0f);
    self.middleName.frame = CGRectMake(self.iconButton.frame.origin.x + self.iconButton.bounds.size.width + offset, offset+self.firstName.frame.origin.y +self.firstName.bounds.size.height, self.bounds.size.width - self.iconButton.frame.origin.x - self.iconButton.bounds.size.width - 2*offset, 40.0f);
    self.email.frame = CGRectMake(offset, offset + self.middleName.frame.origin.y+self.middleName.bounds.size.height, self.bounds.size.width - self.iconButton.frame.origin.x - self.iconButton.bounds.size.width - 2*offset, 40.0f);
    self.phone.frame = CGRectMake(offset, offset + self.email.frame.origin.y+self.email.bounds.size.height, self.bounds.size.width - self.iconButton.frame.origin.x - self.iconButton.bounds.size.width - 2*offset, 40.0f);
    self.country.frame = CGRectMake(offset, offset + self.phone.frame.origin.y+self.phone.bounds.size.height, self.bounds.size.width - self.iconButton.frame.origin.x - self.iconButton.bounds.size.width - 2*offset, 40.0f);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
