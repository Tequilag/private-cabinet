//
//  ImageModel.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "ImageModel.h"
#import "Photos/Photos.h"

NSString *const ImageFilename = @"MyAvatar";

@interface ImageModel () <PHPhotoLibraryChangeObserver>

@property (nonatomic,strong) PHFetchResult *fetchResult;

@end

@implementation ImageModel

-(instancetype)init{
    self = [super init];
    if (self) {
        [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
        _fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:nil];
    }
    return self;
}

-(void)photoLibraryDidChange:(PHChange *)changeInstance{
    PHFetchResultChangeDetails *details = [changeInstance changeDetailsForFetchResult:self.fetchResult];
    if (details){
        self.fetchResult = details.fetchResultAfterChanges;
    }
    if(self.delegate) {
        [self.delegate didUpdateData];
    }
}

-(id) itemForIndexPath:(NSIndexPath *)indexPath{
    return self.fetchResult[indexPath.item];
}

-(NSInteger)countImage{
    return self.fetchResult.count;
}


//- (void)saveImage:(UIImage *)image {
//    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//    NSString *imageName = [NSUUID UUID].UUIDString;
//    
//    [UIImagePNGRepresentation(image) writeToFile:[path stringByAppendingPathComponent:imageName]
//                                      atomically:YES];
//    [NSKeyedArchiver archiveRootObject:imageName toFile:[path stringByAppendingPathComponent:@"ImageFilename"]];
//}
@end
