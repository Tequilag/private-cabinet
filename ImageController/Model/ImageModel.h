//
//  ImageModel.h
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ImageModelDelegate;

@interface ImageModel : NSObject

-(id) itemForIndexPath:(NSIndexPath *)indexPath;

-(NSInteger) countImage;

@property (nonatomic, weak) id<ImageModelDelegate> delegate;

//- (void)saveImage:(UIImage *)image;
@end

@protocol ImageModelDelegate <NSObject>

- (void)didUpdateData;

@end
