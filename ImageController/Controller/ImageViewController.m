//
//  ImageViewController.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageCollectionViewCell.h"
#import "Photos/Photos.h"
#import "ImageModel.h"

NSString* const ImageViewControllerCellRuseIdentifier = @"ImageViewControllerCellRuseIdentifier";

@interface ImageViewController () <UICollectionViewDataSource,UICollectionViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ImageModelDelegate>

@property (nonatomic,strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) ImageModel *model;

@end

@implementation ImageViewController

-(void)loadView{
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    self.view = self.collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[ImageModel alloc]init];
    self.model.delegate =self;
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.flowLayout.itemSize = CGSizeMake(100.0f, 100.0f);
    self.flowLayout.minimumLineSpacing = 5.0f;
    self.flowLayout.minimumInteritemSpacing = 5.0f;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f);
    
    [self.collectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:ImageViewControllerCellRuseIdentifier];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(takePhoto:)];
}

-(void)didUpdateData{
    [self.collectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.countImage;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ImageViewControllerCellRuseIdentifier forIndexPath:indexPath];
    
    id item = [self.model itemForIndexPath:indexPath];
    if ([item isKindOfClass:[PHAsset class]]){
        PHAsset *asset = (PHAsset *) item;
        [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:CGSizeMake(100.0f, 100.0f)
                                                  contentMode:PHImageContentModeDefault
                                                      options:nil
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                    cell.imageView.image = result;
                                                }];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    id item = [self.model itemForIndexPath:indexPath];
    if ([item isKindOfClass:[PHAsset class]]){
        PHAsset *asset = (PHAsset *) item;
        //NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:CGSizeMake(150.0f, 150.0f)
                                                  contentMode:PHImageContentModeDefault
                                                      options:nil
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
//                                                    [UIImagePNGRepresentation(result) writeToFile:[path stringByAppendingPathComponent:self.PhotoFileName]
//                                                                                         atomically:YES];
                                                    [self.delegate LoadImage:result];
                                                    [self.navigationController popViewControllerAnimated:YES];
                                                
                                                }];
    }
}


-(void)takePhoto:(id)sender{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc]init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(nonnull NSDictionary<NSString *,id> *)info{

    UIImage *newImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    [UIImagePNGRepresentation(newImage) writeToFile:[path stringByAppendingPathComponent:self.PhotoFileName]
                                         atomically:YES];

    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
