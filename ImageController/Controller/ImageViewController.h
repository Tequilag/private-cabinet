//
//  ImageViewController.h
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//
#import <UIKit/UIKit.h>

@protocol  LoadImageDelegate;

@interface ImageViewController : UIViewController

@property (nonatomic, strong) NSString *PhotoFileName;
@property(nonatomic,weak) id<LoadImageDelegate> delegate;
@end

@protocol LoadImageDelegate <NSObject>

-(void) LoadImage:(UIImage *) image;

@end
