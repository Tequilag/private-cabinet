//
//  ViewController.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 15.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"
#import "Model.h"
#import "Item.h"
#import "EditViewController.h"

@interface ViewController () < EditViewControllerDelegate>

@property (nonatomic, strong) CustomView *customView;
@property (nonatomic, strong) Model *model;

@end

@implementation ViewController

- (void)loadView {
    self.customView = [[CustomView alloc] init];
    [self setView:self.customView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.model=[[Model alloc]init];
    
    [self.customView.EditInfo addTarget:self action:@selector(EditItem:) forControlEvents:UIControlEventTouchUpInside];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
        self.customView.firstName.text=self.model.item.firstName;
        self.customView.lastName.text=self.model.item.lastName;
        self.customView.middleName.text=self.model.item.middleName;
        self.customView.phone.text=self.model.item.phone;
        self.customView.email.text=self.model.item.email;
        self.customView.country.text=self.model.item.country;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    [self.customView.image setImage:[UIImage imageWithContentsOfFile:[path stringByAppendingPathComponent:self.model.item.imageFileName]]];
    
}
- (void)EditItem:(id)sender {
    EditViewController *editController = [[EditViewController alloc] init];
    editController.delegate = self;
    editController.item=self.model.item;

    [self.navigationController pushViewController:editController animated:YES];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.customView.topOffset = self.topLayoutGuide.length;
}


#pragma mark - EditViewControllerDelegate

- (void)editViewController:(EditViewController *)controller didCreateItem:(Item *)item {
        self.model.item= item;
        [self.model save];
}

//-(Item *)getInfo:(EditViewController *)controller {
//    return self.model.item;
//}
@end


