//
//  Model.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "Model.h"
#import "Item.h"

@implementation Model

- (instancetype)init {
    self = [super init];
    if (self) {
        _item = [[Item alloc] init];
        [self load];
        _item.imageFileName=@"ImageFile";
    }
    return self;
}

- (void)save {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.item];
    
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    [data writeToFile:[path stringByAppendingPathComponent:@"MyFile"] atomically:YES];
}

- (void)load {
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    NSData *data = [NSData dataWithContentsOfFile:[path stringByAppendingPathComponent:@"MyFile"]];
    
    if (data) {
        self.item = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
}

@end

