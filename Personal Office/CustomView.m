//
//  CustomView.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 15.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _image = [[UIImageView alloc] init];
        [self addSubview:_image];

        _EditInfo=[[UIButton alloc] init];
        [_EditInfo setTitle:@"Edit" forState:UIControlStateNormal];
        _EditInfo.backgroundColor=[UIColor grayColor];
        [self addSubview:_EditInfo];
        _firstName = [[UILabel alloc] init];
        //_firstName.text=@"Georgy";
        _firstName.textColor=[UIColor blackColor];
        [self addSubview:_firstName];
        _middleName = [[UILabel alloc] init];
        //_middleName.text=@"Konstantinovich";
        _middleName.textColor=[UIColor blackColor];
                [self addSubview:_middleName];
        _lastName = [[UILabel alloc] init];
        //_lastName.text=@"Gorbenko";
        _lastName.textColor=[UIColor blackColor];
        [self addSubview:_lastName];
        _email = [[UILabel alloc] init];
       // _email.text=@"Tequilagg161@gmail.com";
        _email.textColor=[UIColor blackColor];
        [self addSubview:_email];
        _phone = [[UILabel alloc] init];
//_phone.text=@"+7(988)999-42-00";
        _phone.textColor=[UIColor blackColor];
        [self addSubview:_phone];
        _country = [[UILabel alloc] init];
        //_country.text=@"Russia";
        _country.textColor=[UIColor blackColor];
        [self addSubview:_country];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    CGFloat offset = 10.0f;
    self.image.frame = CGRectMake(offset,offset+ self.topOffset, 150.0f, 150.0f);
    self.lastName.frame = CGRectMake(self.image.frame.origin.x + self.image.bounds.size.width + offset, offset+self.topOffset, self.bounds.size.width - self.image.frame.origin.x - self.image.bounds.size.width - 2*offset, 40.0f);
    self.firstName.frame = CGRectMake(self.image.frame.origin.x + self.image.bounds.size.width + offset, offset+self.lastName.frame.origin.y +self.lastName.bounds.size.height, self.bounds.size.width - self.image.frame.origin.x - self.image.bounds.size.width - 2*offset, 40.0f);
    self.middleName.frame = CGRectMake(self.image.frame.origin.x + self.image.bounds.size.width + offset, offset+self.firstName.frame.origin.y +self.firstName.bounds.size.height, self.bounds.size.width - self.image.frame.origin.x - self.image.bounds.size.width - 2*offset, 40.0f);
    self.email.frame = CGRectMake(offset, offset + self.middleName.frame.origin.y+self.middleName.bounds.size.height, self.bounds.size.width - self.image.frame.origin.x - self.image.bounds.size.width - 2*offset, 40.0f);
    self.phone.frame = CGRectMake(offset, offset + self.email.frame.origin.y+self.email.bounds.size.height, self.bounds.size.width - self.image.frame.origin.x - self.image.bounds.size.width - 2*offset, 40.0f);
    self.country.frame = CGRectMake(offset, offset + self.phone.frame.origin.y+self.phone.bounds.size.height, self.bounds.size.width - self.image.frame.origin.x - self.image.bounds.size.width - 2*offset, 40.0f);
    self.EditInfo.frame = CGRectMake((self.bounds.size.width-50.0f)/2.0f, self.bounds.size.height-offset-40.0f, 50.0f, 40.0f);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

@end
