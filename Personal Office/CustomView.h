//
//  CustomView.h
//  Personal Office
//
//  Created by Gorbenko Georgy on 15.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UIView

@property (nonatomic, strong) UIImageView *image;
@property (nonatomic, strong) UILabel *firstName;
@property (nonatomic, strong) UILabel *lastName;
@property (nonatomic, strong) UILabel *middleName;
@property (nonatomic, strong) UILabel *email;
@property (nonatomic, strong) UILabel *phone;
@property (nonatomic, strong) UILabel *country;
@property (nonatomic, strong) UIButton *EditInfo;

@property (nonatomic, assign) CGFloat topOffset;
@end
