//
//  Model.h
//  Personal Office
//
//  Created by Gorbenko Georgy on 17.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"
@interface Model : NSObject

@property (nonatomic, strong) Item *item;

- (void)save;
- (void)load;

@end
