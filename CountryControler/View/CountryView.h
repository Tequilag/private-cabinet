//
//  CountryView.h
//  Personal Office
//
//  Created by Gorbenko Georgy on 20.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import <UIkit/UIkit.h>

@interface CountryView : UIView

@property (nonatomic, strong) UITableView *table;

@end
