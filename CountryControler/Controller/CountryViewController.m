//
//  CountryViewController.m
//  Personal Office
//
//  Created by Gorbenko Georgy on 20.11.16.
//  Copyright © 2016 Gorbenko Georgy. All rights reserved.
//

#import "CountryViewController.h"
#import "CountryView.h"

@interface CountryViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchResultsUpdating>
@property (nonatomic, strong) CountryView *countryView;
@property (nonatomic, assign)  NSArray * countryArray;
@property (nonatomic, strong) NSMutableArray *sortedCountryArray;
@property (nonatomic, strong) NSArray *searchResult;
@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation CountryViewController
- (void)loadView {
    self.countryView = [[CountryView alloc] init];
    [self setView:self.countryView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.countryView.table.dataSource = self;
    self.countryView.table.delegate = self;
    
    self.countryView.backgroundColor = [UIColor whiteColor];
    NSLocale *locale = [NSLocale currentLocale];
    self.countryArray = [NSLocale ISOCountryCodes];
    
   self.sortedCountryArray = [[NSMutableArray alloc] init];
   
    for (NSString *countryCode in self.countryArray) {
        
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [self.sortedCountryArray addObject:displayNameString];
        
    }
    self.searchResult = [[NSArray alloc] initWithArray:_sortedCountryArray];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.countryView.table.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    self.countryView.table.dataSource = self;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sortedCountryArray.count;
}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MYCell"];
    }
    NSString *str=[self.searchResult objectAtIndex:indexPath.row];
    cell.textLabel.text=str;
    return cell;
}
- (void)searchBar:(UISearchBar * _Nonnull)searchBar
    textDidChange:(NSString * _Nonnull)searchText{
    if(searchText.length){
        NSPredicate* pred = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@",searchText];
        self.searchResult= [_sortedCountryArray filteredArrayUsingPredicate:pred];
        //[self.countryView.table  reloadData ];
    }
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    [self.countryView.table reloadData];
    
}
@end
